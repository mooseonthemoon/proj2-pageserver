from flask import Flask
from flask import render_template
from flask import abort

app = Flask(__name__, static_url_path = '',
	                  static_folder = 'pages',
	                  template_folder = 'pages')

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

@app.errorhandler(403)
def bad_character(e):
	return render_template('403.html'), 403

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<subpath>")
def successful(subpath):
	if subpath == "trivia.html":
		return render_template("trivia.html"), 200
	elif subpath == "trivia.css":
		return render_template("trivia.css"), 200
	elif ".." in subpath or "~" in subpath:
		abort(403, description = "File is forbidden!")
	elif "%2F%2F" in subpath or subpath[0] == "%2F":
		abort(403, description = "File is forbidden!")
	else:
		abort(404, description = "File not found!")

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

